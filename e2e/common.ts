import { Page } from '@playwright/test';
import { expect } from 'playwright-test-coverage'

export const passCaptcha = async (page: Page) => {
    await page.frameLocator('iframe[title="reCAPTCHA"]').getByRole('checkbox', { name: 'I\'m not a robot' }).click();
    // wait for captcha window to disappear
    await expect(page.getByText('Complete CAPTCHA')).toHaveCount(0);
}
