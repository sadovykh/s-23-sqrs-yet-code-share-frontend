import { Flex } from '@chakra-ui/react';
import React from 'react';
import MyHead from './Head';
import Header from './Header';

interface Props {
    children: React.ReactNode;
  }

export default function Page ({
    children,
  }: Props) {
    return (
        <>
        <MyHead/>
        <Flex w="100%" minH="100vh" alignItems="stretch">
          <Flex flexDir="column" flexGrow={ 1 } w={{ base: '100%', lg: 'auto' }}>
              <Header/>
              { children }
          </Flex>
        </Flex>
        </>
    )
  }

