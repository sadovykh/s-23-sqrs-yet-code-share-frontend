
import { Dispatch, SetStateAction } from "react";
import AceEditor from "react-ace";
import "ace-builds/src-noconflict/theme-tomorrow";
import "ace-builds/src-noconflict/theme-monokai";
import { Card, useColorMode } from "@chakra-ui/react";
import "ace-builds/src-noconflict/ext-language_tools";

interface Props {
    language: string,
    content: string,
    setContent: Dispatch<SetStateAction<string>>,
    fontSize?: number,
    readOnly?: boolean,
}

const LANGUAGES = [
    "javascript",
    "java",
    "python",
    "xml",
    "ruby",
    "sass",
    "markdown",
    "mysql",
    "json",
    "html",
    "handlebars",
    "golang",
    "csharp",
    "elixir",
    "typescript",
    "css",
];

LANGUAGES.forEach(lang => {
    require(`ace-builds/src-noconflict/mode-${lang}`);
    require(`ace-builds/src-noconflict/snippets/${lang}`);
});


export default function TextEditor({language, content, setContent, fontSize = 14, readOnly = false}: Props) {
    const { colorMode } = useColorMode();
    return (
        <Card mr={10}>
            <AceEditor
                width="100%"
                placeholder="Paste your code here..."
                mode={language}
                readOnly={readOnly}
                theme={colorMode == "light" ? "tomorrow" : "monokai"}
                name="ace-input"
                onChange={setContent}
                fontSize={fontSize}
                showPrintMargin={false}
                showGutter={true}
                highlightActiveLine={false}
                value={content}
                setOptions={{
                    enableBasicAutocompletion: false,
                    enableLiveAutocompletion: false,
                    enableSnippets: false,
                    showLineNumbers: true,
                    tabSize: 4,
                }}/>
        </Card>
    )
}