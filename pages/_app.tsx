import '@/styles/globals.css'
import type { AppProps } from 'next/app'
import { ChakraProvider, useDisclosure } from '@chakra-ui/react'
import { CaptchaModal } from '@/components/Captcha'
import { useEffect, useState } from 'react'
import { buildCustomFetcher, CustomFetcher } from '@/lib/api'
import Cookies, { CookieAttributes } from 'js-cookie'

export interface CustomPageProps {
  fetcher: CustomFetcher
}

export default function App({ Component, pageProps }: AppProps<CustomPageProps>) {
  const { isOpen: isCaptchaOpen, onOpen: onCaptchaOpen, onClose: onCaptchaClose } = useDisclosure()

  const [captchaToken, setCaptchaToken] = useState<undefined | string>(undefined);

  const fetcher = buildCustomFetcher(onCaptchaOpen);

  pageProps.fetcher = fetcher;

  useEffect(() => {
    // captchaCompleted has changed
    if (captchaToken !== undefined) {
      // Captcha completed
      const call = fetcher.path("/api/user/register").method("get").create();

      const fetchData = async () => {
        const response = await call({ captcha_token: captchaToken });
        const token = response.data.access_token;
        // No HttpOnly because its not too sensitive (it's only a captcha result)
        const tokenCookieAttributes: CookieAttributes = { expires: 1, secure: true, sameSite: "strict" };
        Cookies.set("token", token, tokenCookieAttributes);
        setCaptchaToken(undefined);
      }
      fetchData();
    }
  }, [captchaToken, fetcher])

  return (
    <ChakraProvider>
      <CaptchaModal setCaptchaToken={setCaptchaToken} isCaptchaOpen={isCaptchaOpen} onCaptchaClose={onCaptchaClose} />
      <Component {...pageProps} />
    </ChakraProvider>
  )
}
