import { AbsoluteCenter, Badge, Box, Button, Flex, Heading, HStack, IconButton, Spinner, Stack, Tag, Text, useClipboard, VStack } from '@chakra-ui/react'
import { GetServerSideProps } from 'next'
import { ApiResponse } from 'openapi-typescript-fetch'
import { useEffect, useState } from 'react'
import { components } from '../../lib/schema'
import ErrorPage from 'next/error'
import Page from '@/components/Page'
import dynamic from 'next/dynamic'
import { StarIcon } from '@chakra-ui/icons'

const TextEditor = dynamic(import('@/components/TextEditor'), {
  ssr: false
})


interface Props {
  file_id: string,
  host: string,
  fetcher: any,
}


const FileView = ({ file_id, fetcher }: Props) => {
  const [isLoading, setLoading] = useState(false);
  const [file, setFile] = useState<components["schemas"]["FileInResponse"]>();
  const [err, setError] = useState<{ status: number, msg: any }>();
  const { onCopy: onCopyUrl, setValue: setCurrentUrl, hasCopied: hasCopiedUrl } = useClipboard("");
  const { onCopy: onCopyContent, setValue: setContent, hasCopied: hasCopiedContent } = useClipboard("");

  const updateFile = async () => {
    const call = fetcher.path(`/api/file/${file_id}`).method("get").create();
    call()
      .then((response: ApiResponse<components["schemas"]["FileInResponse"]>) => {
        if (response === undefined) {
          return
        } else if (response.ok) {
          setFile(response.data);
          setContent(response.data.content);
        } else {
          setError({ status: response.status, msg: response.data });
        }
      })
  }

  useEffect(() => {
    updateFile();
    setCurrentUrl(window.location.href)
  }, [file_id, setCurrentUrl, setFile, setContent])

  if (err) {
    return <ErrorPage statusCode={err.status}>{err.msg}</ErrorPage>
  }
  if (file === undefined) {
    return <AbsoluteCenter axis='both'><Spinner size="lg" /></AbsoluteCenter>
  }

  const stared = file.mark == "star";
  const is_private = file.visibility == "private";

  const handleStar = async () => {
    setLoading(true);
    const new_mark = stared ? "no_star" : "star";
    const call = fetcher.path(`/api/file/${file_id}/mark`).method("put").create();
    call({
      file_uid: file_id,
      new_mark: new_mark,
    })
      .then((_: any) => updateFile())
      .finally(() => setLoading(false))
  }

  return (
    <Page>
      <main>
        <VStack spacing={[4, 8]} mt={10} mb={20}>
          <Stack minWidth={[300, 800]} maxWidth={1200}>
            <Flex justifyContent="space-between">
              <VStack>
                <Heading>{file.title}</Heading>
                <HStack>
                  <Tag fontFamily="mono">{file.language.name}</Tag>
                  {is_private && <Badge colorScheme='purple'>private</Badge>}
                </HStack>
              </VStack>
              <HStack>
                <Button colorScheme="linkedin" size="sm" variant={hasCopiedUrl ? 'outline' : 'solid'} onClick={onCopyUrl}>Copy url</Button>
                <Button colorScheme="blue" size="sm" variant={hasCopiedContent ? 'outline' : 'solid'} onClick={onCopyContent}>Copy content</Button>
              </HStack>
            </Flex>
            <TextEditor language={file.language.name} content={file.content} readOnly setContent={() => { }} />
            <Flex justify="right">
              <Button size="sm" colorScheme="telegram" aria-label='Star' onClick={handleStar} isDisabled={isLoading}>
                <HStack>
                  <StarIcon color={stared ? "yellow.200" : "gray.300"} />
                  <Text minW={5}>{file.star_count}</Text>
                </HStack>
              </Button>
            </Flex>
          </Stack>
        </VStack>
      </main>
    </Page>
  )
}

export const getServerSideProps: GetServerSideProps = async ({ query }) => {
  const { id } = query;
  return {
    props: {
      file_id: id,
    }
  }
}

export default FileView
