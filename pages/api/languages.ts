// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'

export type Language = {
  id: number,
  name: string,
}

const languages = [
    {
        "id": 1,
        "name": "javascript"
    },
    {
        "id": 2,
        "name": "java"
    },
    {
        "id": 3,
        "name": "python"
    },
    {
        "id": 4,
        "name": "xml"
    },
    {
        "id": 5,
        "name": "ruby"
    },
    {
        "id": 6,
        "name": "sass"
    },
    {
        "id": 7,
        "name": "markdown"
    },
    {
        "id": 8,
        "name": "mysql"
    },
    {
        "id": 9,
        "name": "json"
    },
    {
        "id": 10,
        "name": "html"
    },
    {
        "id": 11,
        "name": "handlebars"
    },
    {
        "id": 12,
        "name": "golang"
    },
    {
        "id": 13,
        "name": "csharp"
    },
    {
        "id": 14,
        "name": "elixir"
    },
    {
        "id": 15,
        "name": "typescript"
    },
    {
        "id": 16,
        "name": "css"
    },
    {
        "id": 17,
        "name": "rust"
    }
]

export default function handler(
  _req: NextApiRequest,
  res: NextApiResponse<Language[]>
) {
  res.status(200).json(languages)
}
