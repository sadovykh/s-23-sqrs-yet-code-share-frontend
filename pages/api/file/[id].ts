// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'


export const fileState = {
  mark: "no_star"
};

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<any>
) {
  const { id } = req.query
  var star_count = 0;
  var mark = fileState.mark;
  if (fileState.mark === "star") {
    star_count = 1
    fileState.mark = "no_star"
  }
  else {
    star_count = 0
  }
  res.status(200).json({
    "uid": "string",
    "title": "file title",
    "content": `mocked file with id ${id}`,
    "language": {
      "id": 0,
      "name": "language name"
    },
    "visibility": "string",
    "mark": mark,
    "star_count": star_count, 
  })
}
