// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import { components } from '@/lib/schema'

type Token = components["schemas"]["UserTokenInResponse"];


export default function handler(
    _req: NextApiRequest,
    res: NextApiResponse<Token>
) {
    let token: Token = {
        access_token: 'string',
        token_type: 'jwt'
    };
    res.status(200).json(token)
}
